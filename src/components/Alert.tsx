import React from 'react'

import styled from 'styled-components/macro'
import { IconExclamation } from './Icons/IconExclamation'

const Alert = () => {
  return (
    <DivRoot>
      <SpanIcon>
        <IconExclamation />
      </SpanIcon>

      <PText>Sorry, something went wrong :(</PText>
    </DivRoot>
  )
}

export default Alert

const DivRoot = styled.div`
  position: relative;
  color: #ef4444ff;
  display: flex;
`
const SpanIcon = styled.span`
  position: absolute;
  top: 1px;
  left: 0;
  display: flex;
  align-items: center;
`

const PText = styled.p`
  padding-left: 1.2rem;
`
