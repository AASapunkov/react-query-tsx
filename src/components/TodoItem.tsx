import React, { memo, useCallback, useState, FC } from 'react'

import styled from 'styled-components/macro'

import useTodoQuery from '../appData/queries/useTodoQuery'
import useCopy from '../utils/hooks/useCopy'
import { IconDelete } from './Icons/IconDelete'
import { IconUpdate } from './Icons/IconUpdate'
import { FormProps, ITodo } from '../types/types'

interface TodoTypeItemProps {
  id: ITodo['id']
  remove: FormProps['remove']
}

export const TodoItem: FC<TodoTypeItemProps> = memo(({ id, remove }) => {
  const [isMenuVisible, setIsMenuVisible] = useState<boolean>(false)

  const { data } = useTodoQuery(id)
  // TODO уточнить как правильно
  const { isCompleted = false, value = '' } = data || ({} as ITodo)

  const { onCopy, isShowNotice, displayValue } = useCopy(value, value)

  const mouseEnter = useCallback(() => {
    setIsMenuVisible(true)
  }, [])

  const mouseLeave = useCallback(() => {
    setIsMenuVisible(false)
  }, [])

  return (
    <Todo onMouseEnter={mouseEnter} onMouseLeave={mouseLeave}>
      <ContentBox onClick={onCopy}>
        <Content isCompleted={isCompleted}>
          {isShowNotice ? displayValue : value}
        </Content>
      </ContentBox>

      <MenuBox isMenuVisible={isMenuVisible}>
        {isCompleted && (
          <ButtonUpdate
          // onClick={update}
          >
            <IconUpdate />
          </ButtonUpdate>
        )}

        <ButtonRemove onClick={() => remove(id)}>
          <IconDelete />
        </ButtonRemove>
      </MenuBox>
    </Todo>
  )
})

const Todo = styled.li`
  position: relative;
  width: 100%;
  margin-bottom: 0.5rem;
`

const ContentBox = styled.div`
  display: flex;
  padding: 1.5rem;
  border-radius: 20rem;
  background-color: #f9fafbff;
  box-shadow: -1px 7px 9px -5px rgba(0, 0, 0, 0.37);
  //border: 1px solid red;
`

const Content = styled.p<{ isCompleted: boolean }>`
  text-decoration: ${({ isCompleted }) => isCompleted && 'line-through'};
  letter-spacing: 0.025em;
  color: #6b7280ff;
`

const MenuBox = styled.div<{ isMenuVisible: boolean }>`
  opacity: ${({ isMenuVisible }) => (isMenuVisible ? 1 : 0)};
  transition-property: opacity;
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  transition-duration: 150ms;
`

const ButtonUpdate = styled.button`
  position: absolute;
  bottom: 15px;
  left: -35px;
  height: 1.7rem;
  width: 1.7rem;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #6366f1ff;
  color: #fff;
  border: none;

  :focus {
    outline: none;
  }
  :hover {
    background-color: #818cf8ff;
  }
`

const ButtonRemove = styled.button`
  position: absolute;
  bottom: 15px;
  right: -35px;
  height: 1.7rem;
  width: 1.7rem;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #ef4444ff;
  color: #fff;
  border: none;

  :focus {
    outline: none;
  }
  :hover {
    background-color: #f87171ff;
  }
`
