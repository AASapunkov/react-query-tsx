import React from 'react'

import styled, { keyframes } from 'styled-components/macro'
import { IconRefresh } from './Icons/IconRefresh'

const Loading = () => {
  return (
    <DivRoot>
      <IconRefresh />
    </DivRoot>
  )
}

export default Loading

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const DivRoot = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: #818cf8ff;
  animation: ${rotate360} 1s linear infinite;
  transform: translateZ(0);
`
