import React, { memo } from 'react'

import styled from 'styled-components/macro'

import { TodoItem } from './TodoItem'
import { FormProps, TypeTodoIdList } from '../types/types'

interface TodoListProps {
  todoIdList: TypeTodoIdList
  remove: FormProps['remove']
}

const TodoList: React.FC<TodoListProps> = ({ todoIdList, remove }) => {
  return (
    <List>
      {todoIdList.map((id) => (
        <TodoItem key={id} id={id} remove={remove} />
      ))}
    </List>
  )
}

export default memo(TodoList)

const List = styled.ul`
  display: flex;
  width: 100%;
  flex-direction: column;
`
