import React from 'react'
import { useForm } from 'react-hook-form'
import styled from 'styled-components/macro'
import { IconPlus } from './Icons/IconPlus'
import { FormProps, ITodo } from '../types/types'

interface InputTextProps {
  isError?: boolean
}
interface FormProp {
  create: FormProps['create']
}

const Form = ({ create }: FormProp) => {
  const {
    formState: { errors },
    handleSubmit,
    register,
    reset,
  } = useForm()
  // TODO уточнить как правильно
  const onSubmit = (data: Pick<ITodo, 'value'>) => {
    if (errors?.value) return
    create(data)
    reset()
  }

  return (
    <FormRoot onSubmit={handleSubmit(onSubmit)}>
      <InputText
        type="text"
        isError={!!errors?.value}
        {...register('value', {
          required: true,
        })}
        defaultValue=""
        placeholder="Write a new task"
      />

      <ButtonAdd>
        <IconPlus />
      </ButtonAdd>
    </FormRoot>
  )
}

export default Form

const FormRoot = styled.form`
  position: relative;
  margin-top: 2rem;
`

const InputText = styled.input<InputTextProps>`
  display: flex;
  width: 100%;
  padding: 1.4rem;
  border-radius: 20rem;
  background-color: #ffffff;
  box-shadow: -1px 7px 9px -5px rgba(0, 0, 0, 0.37);
  border: ${({ isError }) =>
    `1px solid ${isError ? '#ef4444ff' : '#6b7280ff'}`};
  cursor: pointer;

  :focus {
    outline: none;
    border-color: #6366f1ff;
    box-shadow: 0px 0px 0px 1px #6366f1ff;
  }
  :hover {
    border-color: #6366f1ff;
    box-shadow: 0px 0px 0px 1px #6366f1ff;
  }
`

const ButtonAdd = styled.button`
  position: absolute;
  top: 15px;
  right: 10px;
  height: 2rem;
  width: 2rem;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #6366f1ff;
  color: #fff;
  border: none;

  :focus {
    outline: none;
  }
  :hover {
    background-color: #818cf8ff;
  }
`
