export type TypeTodoIdList = string[]

export interface ITodo {
  id: string
  value: string
  isCompleted: boolean
}

export type FormProps = {
  create: (data: ITodo['value']) => void
  remove: (data: ITodo['id']) => void
  complete: (data: ITodo['isCompleted']) => void
}

type NewType = Omit<ITodo, 'id' | 'isCompleted'>
type NewType2 = Pick<ITodo, 'id' | 'isCompleted'>
