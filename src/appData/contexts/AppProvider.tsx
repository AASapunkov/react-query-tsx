import React, { createContext, useContext, useMemo, ReactNode } from 'react'

import useCompleteTodoMutation from '../mutations/useCompleteTodoMutation'
import useCreateTodoMutation from '../mutations/useCreateTodoMutation'
import useRemoveTodoMutation from '../mutations/useRemoveTodoMutation'
import { ITodo } from '../../types/types'

export type AppContextType = {
  remove: (id: number) => void //  todoIdList
  errorRemove: any // study later error object
  isRemoving: boolean

  create: (data: ITodo['value']) => void // newTodo
  errorCreate: any // study later error object
  isCreating: boolean

  complete: (data: ITodo) => void // res.appData
  errorComplete: any // study later error object
  isCompleting: boolean
}

interface IAppProviderProps {
  children: ReactNode
}

export const AppContext = createContext<AppContextType | null>(null)

export const useApp = () => useContext(AppContext)

// const AppProvider: React.FC<React.ReactElement> =({ children })=> {
export const AppProvider = ({ children }: IAppProviderProps) => {
  const { remove, errorRemove, isRemoving } = useRemoveTodoMutation()
  const { create, errorCreate, isCreating } = useCreateTodoMutation()
  const { complete, errorComplete, isCompleting } = useCompleteTodoMutation()

  const value = useMemo(
    () => ({
      remove,
      errorRemove,
      isRemoving,
      create,
      errorCreate,
      isCreating,
      complete,
      errorComplete,
      isCompleting,
    }),
    [
      create,
      errorCreate,
      errorRemove,
      isCreating,
      isRemoving,
      remove,
      complete,
      errorComplete,
      isCompleting,
    ],
  )

  // TODO уточнить как правильно
  return <AppContext.Provider value={value}>{children}</AppContext.Provider>
}
