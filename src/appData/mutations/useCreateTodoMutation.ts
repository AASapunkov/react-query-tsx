import { useCallback } from 'react'

import axios from 'axios'
import { useMutation, useQueryClient } from 'react-query'

import { qKeyTodo, qKeyTodoList, URL } from '../../constants'
import { TypeTodoIdList, ITodo } from '../../types/types'
import { genId } from '../../utils/customXid'

const useCreateTodoMutation = () => {
  const queryClient = useQueryClient()

  const createMut = useCallback(
    async (data: Pick<ITodo, 'value'>) => {
      try {
        const res = await axios.post(`${URL}/todos`, { ...data, id: genId() })
        const newTodo: ITodo = res.data
        const newTodoId: ITodo['id'] = newTodo.id

        queryClient.setQueryData(
          qKeyTodoList(),
          (cacheData: { todoIdList?: TypeTodoIdList } | undefined) => {
            const { todoIdList = [] } = cacheData || {}

            queryClient.setQueryData(qKeyTodo(newTodoId), newTodo)

            return { todoIdList: [...todoIdList, newTodoId] }
          },
        )

        return newTodo
      } catch (err) {
        throw err
      }
    },
    [queryClient],
  )

  const {
    mutateAsync: create,
    isLoading,
    error,
  } = useMutation(createMut, {
    // onSuccess: () => queryClient.invalidateQueries(qKeyTodoList()),
  })

  return {
    create,
    errorCreate: error,
    isCreating: isLoading,
  }
}

export default useCreateTodoMutation
