import { useCallback } from 'react'
import axios from 'axios'
import { useMutation, useQueryClient } from 'react-query'

import { qKeyTodo, URL } from '../../constants'
import { ITodo } from '../../types/types'

const useCompleteTodoMutation = () => {
  const queryClient = useQueryClient()

  const completeMut = useCallback(
    async (data: Pick<ITodo, 'id' | 'isCompleted'>) => {
      if (data?.isCompleted) return

      try {
        const res = await axios.patch(`${URL}/todos/${data.id}`, {
          ...data,
          isCompleted: true,
        })
        const updatedTodo: ITodo = res.data

        queryClient.setQueryData<ITodo | undefined>(
          qKeyTodo(data.id),
          (cacheData) => {
            if (!cacheData) return

            return {
              ...cacheData,
              isCompleted: updatedTodo.isCompleted,
            }
          },
        )

        return updatedTodo
      } catch (err) {
        throw err
      }
    },
    [queryClient],
  )

  const {
    mutateAsync: complete,
    isLoading,
    error,
  } = useMutation(completeMut, {
    // onSuccess: () => queryClient.invalidateQueries(qKeyTodoList()),
  })

  return {
    complete,
    errorComplete: error,
    isCompleting: isLoading,
  }
}

export default useCompleteTodoMutation
