import { useCallback } from 'react'

import axios from 'axios'
import { useMutation, useQueryClient } from 'react-query'

import { qKeyTodo, qKeyTodoList, URL } from '../../constants'
import { ITodo, TypeTodoIdList } from '../../types/types'

const useRemoveTodoMutation = () => {
  const queryClient = useQueryClient()

  const removeMut = useCallback(
    async (id: ITodo['id']) => {
      try {
        return axios.delete(`${URL}/todos/${id}`).then((res) => {
          queryClient.setQueryData(
            qKeyTodoList(),
            (cacheData: { todoIdList?: TypeTodoIdList } | undefined) => {
              const { todoIdList = [] } = cacheData || {}

              queryClient.removeQueries(qKeyTodo(id))

              return {
                todoIdList: todoIdList.filter((_id) => _id !== id),
              }
            },
          )

          return res.data
        })
      } catch (err) {
        throw err
      }
    },
    [queryClient],
  )

  const {
    mutateAsync: remove,
    isLoading,
    error,
  } = useMutation(removeMut, {
    // onSuccess: () => queryClient.invalidateQueries(qKeyTodoList()),
  })

  return {
    remove,
    errorRemove: error,
    isRemoving: isLoading,
  }
}

export default useRemoveTodoMutation
