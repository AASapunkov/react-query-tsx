import { useQuery } from 'react-query'
import { qKeyTodo } from '../../constants'
import { ITodo } from '../../types/types'

const useTodoQuery = (id: ITodo['id']) => {
  const { data } = useQuery(qKeyTodo(id), () => {})

  return {
    data,
  }
}

export default useTodoQuery
