import React, { useCallback } from 'react'

import { useQueryClient } from 'react-query'
import styled from 'styled-components/macro'

import Alert from './components/Alert'
import Container from './components/Container'
import Form from './components/Form'
import Loading from './components/Loading'
import TodoList from './components/TodoList'
import { qKeyTodo, qKeyTodoList } from './constants'
import useCompleteTodoMutation from './appData/mutations/useCompleteTodoMutation'
import useCreateTodoMutation from './appData/mutations/useCreateTodoMutation'
import useRemoveTodoMutation from './appData/mutations/useRemoveTodoMutation'
import useTodosQuery from './appData/queries/useTodosQuery'
import { ITodo, TypeTodoIdList } from 'src/types/types'

const App = () => {
  const queryClient = useQueryClient()
  const { remove } = useRemoveTodoMutation()
  const { create } = useCreateTodoMutation()
  const { complete } = useCompleteTodoMutation()

  const { data, isError, isLoading } = useTodosQuery()
  const { todoIdList = [] } = data || {}

  const toggleCompleteFirstTask = useCallback(() => {
    // TODO уточнить как правильно
    const cacheData: { todoIdList?: TypeTodoIdList } =
      queryClient.getQueryData(qKeyTodoList()) || {}
    const firstTodoId = cacheData?.todoIdList?.[0]

    if (!firstTodoId) return

    queryClient.setQueryData(qKeyTodo(firstTodoId), (_data) => {
      // TODO уточнить как правильно
      const cacheData: ITodo = _data || {}
      //{ id: "", value: "", isCompleted: false }; // Предположим, что у вас есть значения по умолчанию
      //   const isTodoCompleted = cacheData.isCompleted;
      const isTodoCompleted = cacheData?.isCompleted

      return {
        ...cacheData,
        isCompleted: !isTodoCompleted,
      }
    })
  }, [queryClient])

  return (
    <Container>
      {isError && <Alert />}

      {isLoading && <Loading />}

      <TodoList todoIdList={todoIdList} remove={remove} />

      <Form key="Form-query" create={create} />

      <ButtonToggleComplete
        onClick={toggleCompleteFirstTask}
        type="button"
        value="Toggle Complete first task"
      />
    </Container>
  )
}

export default App

const ButtonToggleComplete = styled.input`
  display: flex;
  margin-top: 1rem;
  width: 100%;
  justify-content: center;
  padding: 1rem;
  border-radius: 20rem;
  background-color: #f9fafbff;
  box-shadow: -1px 7px 9px -5px rgba(0, 0, 0, 0.37);
  color: #6b7280ff;
  border: none;

  :focus {
    outline: none;
  }
  :hover {
    background-color: #f87171ff;
  }
`
