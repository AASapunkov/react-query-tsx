import { useRef, useEffect, useCallback, useMemo, useState } from 'react'

interface CopyHookResult {
  onCopy: () => void
  isShowNotice: boolean
  displayValue: string
}

const useCopy = (value: string, defaultValue: string): CopyHookResult => {
  const [isShowNotice, setIsShowNotice] = useState<boolean>(false)
  const [, copy] = useState<string>('')
  const timeoutIdRef = useRef<number | null>(null)
  // const timeoutId = timeoutIdRef.current

  useEffect(
    () => () => {
      if (timeoutIdRef.current) {
        clearTimeout(timeoutIdRef.current)
      }
    },
    [],
  )

  const displayValue = useMemo(
    () => (isShowNotice ? 'Copied!' : defaultValue),
    [defaultValue, isShowNotice],
  )

  const onCopy = useCallback(() => {
    copy(value)

    setIsShowNotice(true)

    if (timeoutIdRef.current) {
      clearTimeout(timeoutIdRef.current)
    }

    timeoutIdRef.current = window.setTimeout(() => {
      setIsShowNotice(false)
    }, 2000)
  }, [value])

  return { onCopy, isShowNotice, displayValue }
}

export default useCopy
