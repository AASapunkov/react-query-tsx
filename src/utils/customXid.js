// eslint-disable-next-line import/prefer-default-export
export const genId = () => {
  let i
  while (!i) {
    i = Math.random().toString(36).slice(2, 12)
  }

  return i
}
